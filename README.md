# MSE Solver - Vojtěch Petrus #
Zdrojový kód se nachází v adresáři `src/`. Pro spuštění aplikace stačí otevřít soubor `target/MSE-1.0.jar`, buďto dvojklikem, nebo přes příkazovou řádku:
```
#!bash

java -jar target/MSE-1.0.jar
```

Pro ukázku funkčnosti jsou ve složce `sources` videa `vojta_lepsi.mp4`, `vojta_stredni.mp4` a `vojta_horsi.mp4`.

Vytvořeno v rámci předmětu MI-MAI.

Kontakt:

* petruvo1@fit.cvut.cz
* vojtech.petrus@gmail.com