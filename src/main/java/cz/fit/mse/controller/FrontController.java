package cz.fit.mse.controller;

import cz.fit.mse.model.FileNotSupportedException;
import cz.fit.mse.model.FrameCapture;
import cz.fit.mse.model.MSESolver;
import cz.fit.mse.model.Model;
import cz.fit.mse.model.SolverNotReadyException;
import cz.fit.mse.view.AppLayout;
import cz.fit.mse.view.ContentPanel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker.StateValue;
import static javax.swing.SwingWorker.StateValue.DONE;

/**
 * Main controller class.
 *
 * @author etruska
 */
public class FrontController {

    private Model model;
    private AppLayout appLayout;
    private ContentPanel contentPanel;

    public FrontController(Model model) {
        this.model = model;

    }

    /**
     * Called when application layout is ready to be rendered
     *
     * @param layout main application frame
     */
    public void setAppLayout(AppLayout layout) {
        this.appLayout = layout;
        this.contentPanel = new ContentPanel(this, model);
        this.contentPanel.setAppLayout(layout);
    }

    public ContentPanel getContentPanel() {
        return contentPanel;
    }

    public void originalVideoSelected(String filename) {
        if (!model.setOriginalVideo(filename)) {
            appLayout.showStatus("Toto video není podporováno.", AppLayout.STATUS_ERROR);
            contentPanel.getOriginalThumbnail().setDefaultThumbnail();
        } else {
            FrameCapture originalFrameCapture = model.getOriginalFrameCapture();
            contentPanel.getOriginalThumbnail().setThumbnail(originalFrameCapture.getFirstFrameImg());

            int frameCount = Math.max(10, (int) originalFrameCapture.getFrameCount());
            contentPanel.setFrameSliderMax(frameCount);
            contentPanel.setFrameSliderEnabled(true);
        }
    }

    public void distortedVideoSelected(String filename) {
        if (!model.setDistortedVideo(filename)) {
            appLayout.showStatus("Toto video není podporováno.", AppLayout.STATUS_ERROR);
            contentPanel.getDistortedThumbnail().setDefaultThumbnail();
        } else {
            FrameCapture distortedFrameCapture = model.getDistortedFrameCapture();
            contentPanel.getDistortedThumbnail().setThumbnail(distortedFrameCapture.getFirstFrameImg());
        }

    }

    public void computeMSEClicked(int frames) {

        try {
            final MSESolver solver = model.getSolver();
            appLayout.showStatus("Výpočet byl zahájen.", AppLayout.STATUS_INFO);
            solver.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(final PropertyChangeEvent event) {
                    if ("progress".equals(event.getPropertyName())) {
                        contentPanel.setProgress((Integer) event.getNewValue() + 1);
                    } else if ("state".equals(event.getPropertyName())) {
                        if ((StateValue) event.getNewValue() == DONE) {
                            try {
                                double mse = solver.get();
                                contentPanel.setResult(mse);
                                appLayout.showStatus("Výpočet byl dokončen.", AppLayout.STATUS_OK);
                                setAllEnabled(true);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(FrontController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (ExecutionException ex) {
                                Logger.getLogger(FrontController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
            );
            solver.setFrameCount(frames);
            setAllEnabled(false);

            model.startMSESolver();

        } catch (SolverNotReadyException ex) {
            appLayout.showStatus(ex.getMessage(), AppLayout.STATUS_ERROR);
        } catch (FileNotSupportedException ex) {
            appLayout.showStatus(ex.getMessage(), AppLayout.STATUS_ERROR);
        }

    }

    private void setAllEnabled(boolean enabled) {
        contentPanel.setOriginalBtnEnabled(enabled);
        contentPanel.setDistortedBtnEnabled(enabled);
        contentPanel.setFrameSliderEnabled(enabled);
        contentPanel.setMSEBtnEnabled(enabled);
    }

}
