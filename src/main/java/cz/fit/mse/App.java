package cz.fit.mse;

import cz.fit.mse.controller.FrontController;
import cz.fit.mse.model.Model;
import cz.fit.mse.view.AppLayout;
import javax.swing.SwingUtilities;

public class App {

    public static void main(String[] args) throws Exception {

        final Model model = new Model();
        final FrontController controller = new FrontController(model);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                new AppLayout(model, controller).setVisible(true);
            }
        });

    }

}
