package cz.fit.mse.view;

import cz.fit.mse.controller.FrontController;
import cz.fit.mse.model.Model;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Content panel.
 * @author etruska
 */
public class ContentPanel extends JPanel {
    
    private JButton selectOriginalBtn;
    private JButton selectDistortedBtn;
    private JButton computeMSEBtn;
    private JFileChooser fc;
    private JProgressBar progressBar;
    private AppLayout appLayout;
    private FrontController controller;
    private Model model;
    private ThumbnailPanel originalThumbnail;
    private ThumbnailPanel distortedThumbnail;
    private JSlider frameSlider;
    private JLabel frameLabel;
    private JLabel resultLabel;
    
    public ContentPanel(FrontController con, Model model) {
        
        this.controller = con;
        this.model = model;
        
        this.setLayout(new GridBagLayout());
        this.selectOriginalBtn = new JButton("Vybrat původní video");
        this.selectDistortedBtn = new JButton("Vybrat upravené video");
        this.computeMSEBtn = new JButton("Vypočítat MSE");
        this.fc = new JFileChooser();
        this.frameSlider = new JSlider(JSlider.HORIZONTAL, 1, 100, 1);
        frameSlider.setEnabled(false);
        frameSlider.setPaintTicks(true);
        frameSlider.setPaintLabels(true);
        
        this.frameLabel = new JLabel("Snímků: 1");
        
        initComponents();
        setHandlers();
    }
    
    public AppLayout getAppLayout() {
        return appLayout;
    }
    
    public void setAppLayout(AppLayout appLayout) {
        this.appLayout = appLayout;
    }
    
    private void initComponents() {
        
        placeThumbnails();
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.insets = new Insets(10, 10, 10, 10);
        // c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        this.add(selectOriginalBtn, c);
        
        c.insets = new Insets(10, 0, 0, 10);
        c.gridx = 2;
        c.gridy = 1;
        c.gridwidth = 2;
        this.add(selectDistortedBtn, c);
        
        placeProgressBar();
        
        placeButtons();
        
    }
    
    private void setHandlers() {
        
        this.selectOriginalBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int returnVal = fc.showOpenDialog(ContentPanel.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String filePath = fc.getSelectedFile().getAbsolutePath();
                    controller.originalVideoSelected(filePath);
                }
            }
        });
        
        this.selectDistortedBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int returnVal = fc.showOpenDialog(ContentPanel.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String filePath = fc.getSelectedFile().getAbsolutePath();
                    controller.distortedVideoSelected(filePath);
                }
            }
        });
        
        this.computeMSEBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.computeMSEClicked(frameSlider.getValue());
            }
        });
        
        this.frameSlider.addChangeListener(new ChangeListener() {
            
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                setMSEButtonFrames(source.getValue());
            }
        });
        
    }
    
    private void placeThumbnails() {
        GridBagConstraints c = new GridBagConstraints();
        
        c.insets = new Insets(10, 10, 5, 10);
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 0;
        
        this.originalThumbnail = new ThumbnailPanel(true);
        this.distortedThumbnail = new ThumbnailPanel(false);
        this.add(originalThumbnail, c);
        
        c.insets = new Insets(10, 5, 10, 10);
        c.gridx = 2;
        this.add(distortedThumbnail, c);
    }
    
    private void placeProgressBar() {
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 10);
        c.gridwidth = 4;
        c.gridx = 0;
        c.gridy = 2;
        this.add(progressBar, c);
    }
    
    private void placeButtons() {
        
        GridBagConstraints c = new GridBagConstraints();
        
        c.insets = new Insets(0, 0, 0, 0);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 3;
        this.add(frameSlider, c);
        
        c.insets = new Insets(10, 8, 20, 10);
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.ipady = 10;
        c.gridx = 2;
        c.gridwidth = 1;
        
        c.gridy = 3;
        this.add(computeMSEBtn, c);
        
        this.resultLabel = new JLabel(" ");
        resultLabel.setOpaque(true);
        Font font = resultLabel.getFont();
        Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
        resultLabel.setFont(boldFont);
        
        c.insets = new Insets(15, 8, 20, 10);
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 3;
        this.add(resultLabel, c);
    }
    
    public ThumbnailPanel getOriginalThumbnail() {
        return originalThumbnail;
    }
    
    public ThumbnailPanel getDistortedThumbnail() {
        return distortedThumbnail;
    }
    
    public void setResult(double value) {
        this.resultLabel.setText(Double.toString(value));
    }
    
    public void setProgress(int value) {
        progressBar.setValue(value);
    }
    
    public void setMSEBtnEnabled(boolean enabled) {
        this.computeMSEBtn.setEnabled(enabled);
    }
    
    public void setOriginalBtnEnabled(boolean enabled) {
        this.selectOriginalBtn.setEnabled(enabled);
    }
    
    public void setDistortedBtnEnabled(boolean enabled) {
        this.selectDistortedBtn.setEnabled(enabled);
    }
    
    public void setFrameSliderEnabled(boolean enabled) {
        this.frameSlider.setEnabled(enabled);
    }
    
    public void setFrameSliderMax(int max) {
        
        int dist = max / 7;
        this.frameSlider.setMaximum(max);
        
        Hashtable labelTable = new Hashtable();
        labelTable.put(new Integer(1), new JLabel("1"));
        
        for (int i = dist; i < max; i += dist) {
            if (i + 20 < max) //to ensure safe distance between last two labels
            {
                labelTable.put(new Integer(i), new JLabel(Integer.toString(i)));
            }
            
        }
        
        labelTable.put(new Integer(max), new JLabel(Integer.toString(max)));
        this.frameSlider.setLabelTable(labelTable);
    }
    
    private void setMSEButtonFrames(int frames) {
        String framesText = "snímků";
        if (frames == 1) {
            framesText = "snímek";
        } else if (frames > 1 && frames < 5) {
            framesText = "snímky";
        }
        
        computeMSEBtn.setText("MSE pro " + frames + " " + framesText);
        
    }
    
}
