package cz.fit.mse.view;

import cz.fit.mse.model.Config;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Panel for placing image thumbnails.
 *
 * @author etruska
 */
public class ThumbnailPanel extends JLabel {

    private BufferedImage thumbnail;

    private boolean original;

    public ThumbnailPanel(boolean orig) {
        this.initialize(orig);
        setDefaultThumbnail();
    }

    public ThumbnailPanel(BufferedImage image, boolean orig) {
        this.initialize(orig);
        setThumbnail(image);
    }

    private void initialize(boolean orig) {
        this.setSize(Config.THUMBNAIL_WIDTH, Config.THUMBNAIL_HEIGHT);
        this.setMinimumSize(new Dimension(Config.THUMBNAIL_WIDTH, Config.THUMBNAIL_HEIGHT));
        this.setPreferredSize(new Dimension(Config.THUMBNAIL_WIDTH, Config.THUMBNAIL_HEIGHT));
        this.setMaximumSize(new Dimension(Config.THUMBNAIL_WIDTH, Config.THUMBNAIL_HEIGHT));
        this.original = orig;
    }

    public final void setDefaultThumbnail() {
        if (original) {
            setThumbnail(Config.ORIGINAL_THUMBNAIL);
        } else {
            setThumbnail(Config.DISTORTED_THUMBNAIL);
        }

    }

    public final void setThumbnail(URL url) {
        try {
            thumbnail = ImageIO.read(url);
            displayThumbnail();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public final void setThumbnail(BufferedImage image) {
        this.thumbnail = image;
        displayThumbnail();
    }

    private void displayThumbnail() {
        Dimension newDimension = getScaledDimension(thumbnail.getWidth(), thumbnail.getHeight(), Config.THUMBNAIL_WIDTH, Config.THUMBNAIL_HEIGHT);
        this.setIcon(new ImageIcon(thumbnail.getScaledInstance(newDimension.width, newDimension.height, Image.SCALE_DEFAULT)));

    }

    private Dimension getScaledDimension(int originalWidth, int originalHeight, int boundWidth, int boundHeight) {

        int newWidth = originalWidth;
        int newHeight = originalHeight;

        // first check if we need to scale width
        if (originalWidth > boundWidth) {
            //scale width to fit
            newWidth = boundWidth;
            //scale height to maintain aspect ratio
            newHeight = (newWidth * originalHeight) / originalWidth;
        }

        // then check if we need to scale even with the new height
        if (newHeight > boundHeight) {
            //scale height to fit instead
            newHeight = boundHeight;
            //scale width to maintain aspect ratio
            newWidth = (newHeight * originalWidth) / originalHeight;
        }

        return new Dimension(newWidth, newHeight);
    }

}
