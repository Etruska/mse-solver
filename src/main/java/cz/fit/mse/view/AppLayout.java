package cz.fit.mse.view;

import cz.fit.mse.model.Config;
import cz.fit.mse.controller.FrontController;
import cz.fit.mse.model.Model;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Main window with application layout.
 *
 * @author Vojtěch Petrus
 */
public class AppLayout extends JFrame {

    private JPanel contentBoard;
    private JLabel statusBar;

    public static final int STATUS_OK = 1;
    public static final int STATUS_ERROR = 2;
    public static final int STATUS_INFO = 3;

    private FrontController controller;
    private Model model;
    private Timer timer;

    public AppLayout(Model model, FrontController con) {
        try {

            this.model = model;
            this.controller = con;
            this.controller.setAppLayout(this);
            this.timer = new Timer();
            
            this.setLayout(new BorderLayout());

            this.setSize(new Dimension(Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT));
            int[] position = getCenterPosition(Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT);
            this.setLocation(position[0], position[1]);

            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setTitle("MSE Solver");
            this.setResizable(true);

            this.contentBoard = this.controller.getContentPanel();
            this.add(contentBoard, BorderLayout.CENTER);

            this.statusBar = new JLabel("Vyberte videa pro výpočet mean square error (MSE).");
            this.statusBar.setName("stausBar");
            this.statusBar.setBackground(Color.white);
            this.statusBar.setOpaque(true);
            this.statusBar.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            this.add(this.statusBar, BorderLayout.SOUTH);

        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }

    }

    public void modelUpdated() {
    }

    public void showStatus(String text, int type) {
        timer.cancel();
        timer = new Timer();

        this.statusBar.setText(text);

        Font font = this.statusBar.getFont();
        Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
        this.statusBar.setFont(boldFont);

        switch (type) {
            case STATUS_OK:
                this.statusBar.setForeground(Config.COLOR_GREEN);
                break;
            case STATUS_ERROR:
                this.statusBar.setForeground(Config.COLOR_RED);
                break;
            case STATUS_INFO:
                this.statusBar.setForeground(Config.COLOR_BLUE);
                break;
        }


        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                clearStatus();
            }
        }, Config.STATUS_DURATION
        );
    }

    public String getStatus() {
        return this.statusBar.getText();
    }

    public void clearStatus() {
        this.statusBar.setText(" ");
    }

    public static int[] getCenterPosition(int width, int height) {
        int[] position = new int[2];
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

        position[0] = (int) (screen.getWidth() / 2 - (width / 2));
        position[1] = (int) (screen.getHeight() / 2 - (height / 2));
        return position;
    }
}
