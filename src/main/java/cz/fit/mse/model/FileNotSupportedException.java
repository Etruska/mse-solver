package cz.fit.mse.model;

/**
 * 
 * @author etruska
 */
public class FileNotSupportedException extends Exception {
    public FileNotSupportedException(String message) {
        super(message);
    }
}