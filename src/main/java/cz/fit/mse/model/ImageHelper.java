package cz.fit.mse.model;

import cz.fit.mse.model.Config;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Helpers for BufferedImage objects.
 * @author etruska
 */
public class ImageHelper {

    static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    public void writeText(String text, BufferedImage image) {
        Graphics g = image.getGraphics();
        g.setFont(g.getFont().deriveFont(20f));
        g.setColor(Color.blue);
        g.drawString(text, 100, 100);
        g.dispose();
    }

    public void dumpImage(BufferedImage originalImage, int frameNumber, String filename) {
        try{
            BufferedImage image = deepCopy(originalImage);
            writeText(frameNumber + ".", image);
            ImageIO.write(image, "png", new File(Config.DEBUG_FOLDER + "/" + frameNumber + "_" + filename + ".png"));
        
        } catch(IOException ex){
            System.err.println(ex.getMessage());
        }
    }

    public void dumpImage(BufferedImage image, int frameNumber) {
        dumpImage(image, frameNumber, "image");
    }
    
    public void dotImage(BufferedImage image, int frameNumber){
        
        Color myColor = new Color(0, 255, 255);
        int rgb = myColor.getRGB();
        
        for(int i = 0; i < frameNumber; i++){
            if(i < image.getHeight() && i < image.getWidth()){
                image.setRGB(i, i, rgb);
            }
                    
        }
    }
}
