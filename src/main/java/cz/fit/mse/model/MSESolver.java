package cz.fit.mse.model;

import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.swing.SwingWorker;

/**
 *
 * @author etruska
 */
public class MSESolver extends SwingWorker<Double, Void> {

    private int imgWidth = 0;
    private int imgHeight = 0;
    private ImageHelper iHelper;
    private FrameCapture originalVid;
    private FrameCapture distortedVid;
    private long frameCount;

    public MSESolver(ImageHelper iHelper, FrameCapture originalVid, FrameCapture distortedVid) {
        this.iHelper = iHelper;
        this.originalVid = originalVid;
        this.distortedVid = distortedVid;
    }

    public double getFrameMSE(BufferedImage img1, BufferedImage img2) {
        imgWidth = img1.getWidth();
        imgHeight = img1.getHeight();

        double error = 0;

        for (int i = 0; i < imgWidth; i++) {
            for (int j = 0; j < imgHeight; j++) {
                double lum1 = getLuminance(new Color(img1.getRGB(i, j)));
                double lum2 = getLuminance(new Color(img2.getRGB(i, j)));
                error += Math.pow(lum1 - lum2, 2);
            }
        }

        return error;
    }

    private double getLuminance(Color color) {
        return (color.getRed() * 0.2126) + (color.getGreen() * 0.7152) + (color.getBlue() * 0.0722);
    }

    @Override
    protected Double doInBackground() throws Exception {

        double error = 0;
        BufferedImage originalFrame, distortedFrame;

        for (int frameNumber = 0; frameNumber < frameCount; frameNumber++) {

            originalFrame = originalVid.getNextFrame();
            distortedFrame = distortedVid.getNextFrame();

            //iHelper.dotImage(originalFrame, frameNumber);
            //iHelper.dumpImage(originalFrame, frameNumber);
            //iHelper.dumpImage(distortedFrame, frameNumber, "distorted");

            double frameMSE = getFrameMSE(originalFrame, distortedFrame);
            
            //System.out.println(frameNumber + ". frameMSE: " + frameMSE);
            error += frameMSE;
            
            double percent = this.frameCount / 100.0;
            //frameNumber starts with 0
            int progress = (int) ((frameNumber+1) / percent);
            this.setProgress(Math.max(0, Math.min(100, progress)));
        }
        double constant = (1.0 / (imgWidth * imgHeight * frameCount));
        return error * constant;
    }

    public void setFrameCount(long frameCount) {
        this.frameCount = frameCount;
    }

    
}
