package cz.fit.mse.model;

/**
 * Interface for all objects listening to worker changes.
 * @author Vojtěch Petrus
 */
public interface WorkerObserver {
    
    public void workerFinished();
    
}
