package cz.fit.mse.model;

import java.awt.Color;
import java.net.URL;

/**
 * Static configuration of application parameters
 *
 * @author Vojtěch Petrus
 */
public class Config {

    public static final String SOURCES_FOLDER = "sources";
    public static final String DEBUG_FOLDER = "debug";
  
   // public static final String ORIGINAL_THUMBNAIL = THUMBNAIL_FOLDER + "/movie-icon-original.png";
    public static final URL ORIGINAL_THUMBNAIL = Config.class.getResource("/images/movie-icon-original.png");
   // public static final String DISTORTED_THUMBNAIL = THUMBNAIL_FOLDER + "/movie-icon-distorted.png";
    public static final URL DISTORTED_THUMBNAIL = Config.class.getResource("/images/movie-icon-distorted.png");

    public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 540;

    public static final int THUMBNAIL_WIDTH = 360;
    public static final int THUMBNAIL_HEIGHT = 280;

    public static final int STATUS_DURATION = 10000;
    //I don't like the default green
    public static final Color COLOR_GREEN = new Color(121, 196, 48);
    public static final Color COLOR_BLUE = Color.BLUE;
    public static final Color COLOR_RED = Color.RED;
}
