package cz.fit.mse.model;

/**
 * Interface for all objects listening to model changes.
 * @author Vojtěch Petrus
 */
public interface ModelObserver {
    
    public void modelUpdated();
    
}
