package cz.fit.mse.model;

/**
 * Main model class.
 * @author etruska
 */
public class Model {

    private MSESolver solver;
    private boolean solverReady;
    private final ImageHelper iHelper;

    private FrameCapture originalFrameCapture;
    private FrameCapture distortedFrameCapture;

    public final static int READY = 1;
    public final static int VID_MISSING = 2;
    public final static int WRONG_RESOLUTION = 3;

    public Model() {
        this.iHelper = new ImageHelper();
        this.originalFrameCapture = null;
        this.distortedFrameCapture = null;
    }

    public void startMSESolver() {
        solver.execute();
    }

    public boolean setOriginalVideo(String path) {
        try {
            originalFrameCapture = new FrameCapture(path);

            if (validateVideo(originalFrameCapture)) {
                return true;
            }
            originalFrameCapture = null;
        } catch (FileNotSupportedException ex) {
            originalFrameCapture = null;
            System.err.println(ex.getMessage());
        }
        return false;

    }

    public boolean setDistortedVideo(String path) {
        try {
            distortedFrameCapture = new FrameCapture(path);

            if (validateVideo(distortedFrameCapture)) {
                return true;
            }
            distortedFrameCapture = null;
        } catch (FileNotSupportedException ex) {
            distortedFrameCapture = null;
            System.err.println(ex.getMessage());
        }
        return false;
    }

    private boolean validateVideo(FrameCapture video) {
        return video.getFrameCount() > 0;
    }

    public FrameCapture getOriginalFrameCapture() {
        return originalFrameCapture;
    }

    public FrameCapture getDistortedFrameCapture() {
        return distortedFrameCapture;
    }

    public MSESolver getSolver() throws SolverNotReadyException, FileNotSupportedException {
        if (originalFrameCapture == null || distortedFrameCapture == null) {
            throw new SolverNotReadyException("Musí být vybrány obě videa.");
        }
        if (originalFrameCapture.getFrameCount() != distortedFrameCapture.getFrameCount()) {
            throw new SolverNotReadyException("Obě videa musí mít stejné rozměry.");
        }

        originalFrameCapture = new FrameCapture(originalFrameCapture.getVideoFile());
        distortedFrameCapture = new FrameCapture(distortedFrameCapture.getVideoFile());
        //originalFrameCapture.rewind();
        //distortedFrameCapture.rewind();
        solver = new MSESolver(iHelper, originalFrameCapture, distortedFrameCapture);
        return solver;
    }

    public boolean isSolverReady() {
        return solverReady;
    }

}
