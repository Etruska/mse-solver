package cz.fit.mse.model;

import java.awt.image.BufferedImage;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.mediatool.event.IVideoPictureEvent;
import com.xuggle.xuggler.IContainer;
import java.util.Arrays;

/**
 * Loads video, collects its information and provides frame-by-frame capturing.
 * @author aclarke
 * @author trebor
 * @author Vojtěch Petrus
 */
public class FrameCapture extends MediaListenerAdapter {

    private int mVideoStreamIndex = -1;
    private int capturedFrames = 0;
    private IMediaReader reader;
    private BufferedImage currentFrameImg = null;
    private BufferedImage firstFrameImg = null;
    private String videoFile;
    private boolean infoCollected;
    private long frameCount;
    private int videoWidth;
    private int videoHeight;
    private String allowedExt[];

    /**
     * Construct a DecodeAndCaptureFrames which reads and captures frames from a
     * video file.
     *
     * @param videoFile
     * @param filename the name of the media file to read
     * @throws cz.fit.mse.model.FileNotSupportedException
     */
    public FrameCapture(String videoFile) throws FileNotSupportedException {
        this.allowedExt = new String[]{"mpg", "mpeg", "mp4", "avi", "flv", "wmv"};
        // create a media reader for processing video
        this.videoFile = videoFile;
        this.infoCollected = false;

        checkExtension(videoFile);
        // capture first frame to get video information
        collectVideoInfo();

        //then reset reader and wait for orders
        reader = ToolFactory.makeReader(videoFile);
        // stipulate that we want BufferedImages created in BGR 24bit color space
        reader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);

        //reader.addListener(ToolFactory.makeViewer());
        reader.addListener(this);
    }

    private void checkExtension(String fileName) throws FileNotSupportedException {
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            String extension = fileName.substring(i + 1);
            if(Arrays.asList(allowedExt).contains(extension))
                return;
        }
        throw new FileNotSupportedException("Soubor " + fileName + " nemá podporovaný formát.");
    }

    

    public void rewind() {
        capturedFrames = 0;
        //reader.getContainer().seekKeyFrame(mVideoStreamIndex, -1, 0);
        reader.getContainer().seekKeyFrame(mVideoStreamIndex, 0, IContainer.SEEK_FLAG_FRAME);
    }

    private void collectVideoInfo() {
        reader = ToolFactory.makeReader(videoFile);
        reader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
        reader.addListener(this);

        while (reader.readPacket() == null && !infoCollected);

    }

    public BufferedImage getNextFrame() {

        int startingFrame = capturedFrames;

        while (reader.readPacket() == null && capturedFrames == startingFrame);

        return currentFrameImg;
    }

    /**
     * Called after a video frame has been decoded from a media stream.
     * Optionally a BufferedImage version of the frame may be passed if the
     * calling {@link IMediaReader} instance was configured to create
     * BufferedImages.
     *
     * This method blocks, so return quickly.
     *
     * @param event
     */
    @Override
    public void onVideoPicture(IVideoPictureEvent event) {
        try {
            // if the stream index does not match the selected stream index,
            // then have a closer look

            if (event.getStreamIndex() != mVideoStreamIndex) {
                // if the selected video stream id is not yet set, go ahead an
                // select this lucky video stream

                if (-1 == mVideoStreamIndex) {
                    mVideoStreamIndex = event.getStreamIndex();
                } // otherwise return, no need to show frames from this video stream
                else {
                    return;
                }
            }

            if (!infoCollected) {

                frameCount = reader.getContainer().getStream(mVideoStreamIndex).getNumFrames();
                firstFrameImg = ImageHelper.deepCopy(event.getImage());
                videoWidth = firstFrameImg.getWidth();
                videoHeight = firstFrameImg.getHeight();
                infoCollected = true;
            } else {
                currentFrameImg = event.getImage();
                capturedFrames++;
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in onVideoPicture: " + e.getMessage());
        }
    }

    public BufferedImage getFirstFrameImg() {
        return firstFrameImg;
    }

    public long getFrameCount() {
        return frameCount;
    }

    public int getVideoWidth() {
        return videoWidth;
    }

    public int getVideoHeight() {
        return videoHeight;
    }

    public int getCapturedFrames() {
        return capturedFrames;
    }

    public String getVideoFile() {
        return videoFile;
    }

}
