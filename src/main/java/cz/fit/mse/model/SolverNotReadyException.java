package cz.fit.mse.model;

/**
 * Thrown when MSE Solver is not ready.
 * @author etruska
 */
public class SolverNotReadyException extends Exception {
    public SolverNotReadyException(String message) {
        super(message);
    }
}